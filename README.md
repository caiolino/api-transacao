**Introdução**

API REST que recebe Transações e dá estatísticas.

**Endpoints**

1. POST /transacao

Este é o endpoint que irá receber as Transações. Cada transação consiste de um valor e
uma dataHora de quando ela aconteceu:

{

"valor": 123.45,
"dataHora": "2020-08-07T10:11:12.000Z"

}

2. DELETE /transacao

Esta requisição simplesmente apaga todos os dados de transação que estejam
armazenados.

3. GET /estatistica

Este endpoint deve retornar estatísticas das transações que aconteceram apenas nos
últimos 60 segundos (1 minuto). Veja a seguir um exemplo de resposta esperado desse
endpoint:

{

"count": 10,
"sum": 1234.56,
"avg": 123.456,
"min": 12.34,
"max": 123.56

}

4. GET /transacao

Este endpoint deve retornar as transações que aconteceram. Veja a seguir um exemplo de resposta esperado desse
endpoint:

[
  {
    "valor": 58.951,
    "dataHora": "2020-08-07T10:11:12",
    "transactionDateEntry": "2020-09-11T14:45:45.157274"
  },
  {
    "valor": 58.951,
    "dataHora": "2020-08-07T10:11:12",
    "transactionDateEntry": "2020-09-11T14:45:46.312649"
  }
]