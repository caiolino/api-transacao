package com.financeapi.financeapi.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.financeapi.financeapi.factory.TransactionFactory;
import com.financeapi.financeapi.factory.impl.TransactionFactoryImpl;
import com.financeapi.financeapi.model.Statistic;
import com.financeapi.financeapi.model.Transaction;

@Service
public class TransactionService {
	
 	private TransactionFactory factory; 
	private List<Transaction> transactions;
	
	public void createFactory() {
		if(factory == null) {
			factory = new TransactionFactoryImpl();
		}
	}
	
	public void createTransactionList() {
		if(transactions == null) {
			transactions = new ArrayList<>();
		}
	}

	public boolean isJSONValid(String jsonInString) {
	    try {
	       final var mapper = new ObjectMapper();
	       mapper.readTree(jsonInString);
	       return true;
	    } catch (IOException e) {
	       return false;
	    }
	}
	
	
	private BigDecimal parseAmount(JSONObject transaction) {
		return new BigDecimal((String) transaction.get("valor"));
	}
	
	private LocalDateTime parseTransactionDate(JSONObject transaction) {
		String transactionDate = (String) transaction.get("transactionDate");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTime = LocalDateTime.parse(transactionDate, formatter);
		return dateTime;
	}
	
	public boolean isTransactionInFuture(Transaction transaction) {
		return transaction.getTransactionDate().isAfter(LocalDateTime.now());
	}
	
	public boolean isTransactionBiggerThanZero(Transaction transaction) {
		if(transaction.getValor().compareTo(BigDecimal.ZERO) >=0 ) {
			return true;
		}else {
			return false;
		}
	}
	
	private void setTransactionValues(JSONObject jsonTransaction, Transaction transaction) {	
		transaction.setValor(jsonTransaction.get("valor") != null ? parseAmount(jsonTransaction) : transaction.getValor());
		transaction.setTransactionDate(jsonTransaction.get("transactionDate") != null ? 
				parseTransactionDate(jsonTransaction) : transaction.getTransactionDate());
	}
	
	
	public Transaction create(JSONObject jsonTransaction) {
		
		createFactory();

		Transaction transaction = factory.createTransaction();
		setTransactionValues(jsonTransaction, transaction);
		
		return transaction;
	}
	
	
	public void add(Transaction transaction) {
		createTransactionList();
		transactions.add(transaction);
	}
	
	public List<Transaction> find() {
		createTransactionList();
		return transactions;
	}
	
	public boolean delete() {
		transactions.clear();
		return transactions.isEmpty();
	}
	
	public List<Transaction> getAllTransactionsLastMinute(List<Transaction> transactions) {
		return transactions.stream().filter(t -> Duration.between(t.getTransactionDateEntry(), LocalDateTime.now())
				.toMillis() <= 60000).collect(Collectors.toList());
	}

	
	public Statistic statsCreate(List<Transaction> transactions) {
		
		Statistic statistics = new Statistic();
		statistics.setCount(transactions.stream().count());
		statistics.setAvg(BigDecimal.valueOf(transactions.stream().mapToDouble(t -> t.getValor().doubleValue()).average().orElse(0.0))
				.setScale(2, RoundingMode.HALF_UP));
		statistics.setMin(BigDecimal.valueOf(transactions.stream().mapToDouble(t -> t.getValor().doubleValue()).min().orElse(0.0))
				.setScale(2, RoundingMode.HALF_UP));
		statistics.setMax(BigDecimal.valueOf(transactions.stream().mapToDouble(t -> t.getValor().doubleValue()).max().orElse(0.0))
				.setScale(2, RoundingMode.HALF_UP));
		statistics.setSum(BigDecimal.valueOf(transactions.stream().mapToDouble(t -> t.getValor().doubleValue()).sum())
				.setScale(2, RoundingMode.HALF_UP));
		
		return statistics;
	}

}
