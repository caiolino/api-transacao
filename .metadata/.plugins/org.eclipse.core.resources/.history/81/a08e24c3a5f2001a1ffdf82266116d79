package com.financeapi.financeapi.ut;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.financeapi.financeapi.model.Statistic;
import com.financeapi.financeapi.model.Transaction;
import com.financeapi.financeapi.service.TransactionService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TransactionService.class })
@EnableConfigurationProperties
public class FinanceUnitTests {
	
	@Autowired
	private TransactionService transactionService;
	

	@Test
	public void shouldReturnNotNullTransactionService() {
		assertNotNull(transactionService);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void shouldReturnTransactionCreatedWithSuccess() throws Exception {
		
		LocalDateTime now = LocalDateTime.now();
		
		String localDate = now.toString();
		
		JSONObject json = new JSONObject();
		json.put("valor", "22.88");
		json.put("dataHora", localDate);
		
		Transaction transaction = transactionService.create(json);
		
		assertNotNull(transaction);
		assertEquals(transaction.getValor().toString(), json.get("valor"));
		assertEquals(localDate, json.get("dataHora"));
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void shouldReturnTransactionCreatedInFuture() throws Exception {
		
		JSONObject json = new JSONObject();
		json.put("valor", "22.88");
		json.put("dataHora", "2030-08-07T10:11:12.000");
		
		Transaction transaction = transactionService.create(json);
		boolean transactionInFuture = transactionService.isTransactionInFuture(transaction);
		
		assertTrue(transactionInFuture);
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void isTransactionBiggerThanZero() throws Exception {
		
		JSONObject json = new JSONObject();
		json.put("valor", "22.88");
		json.put("dataHora", "2010-08-07T10:11:12.000");
		
		Transaction transaction = transactionService.create(json);
		boolean bigger = transactionService.isTransactionBiggerThanZero(transaction);
		
		assertTrue(bigger);
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void shouldReturnTransactionStatisticsCalculated() throws Exception {
				
		LocalDateTime now = LocalDateTime.now();		
		String localDate = now.toString();
		
		JSONObject json1 = new JSONObject();
		json1.put("valor", "22.88");
		json1.put("dataHora", localDate);
		
		Transaction transaction = transactionService.create(json1);
		transactionService.add(transaction);
		
		JSONObject json = new JSONObject();
		json.put("valor", "120.0");
		json.put("dataHora", "2010-08-07T10:11:12.000");
		
		transaction = transactionService.create(json);
		transactionService.add(transaction);
		
		Statistic statistic = transactionService.statsCreate(transactionService.find());
		
		assertNotNull(statistic);
		assertEquals("142.88", statistic.getSum().toString());
		assertEquals("71.44", statistic.getAvg().toString());
		assertEquals("22.88", statistic.getMin().toString());
		assertEquals("120.00", statistic.getMax().toString());
		assertEquals(2, statistic.getCount());
	}

}
