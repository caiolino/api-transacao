package com.financeapi.financeapi.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.financeapi.financeapi.model.Transaction;

@Service
public class TransactionService {
	
	private List<Transaction> transactions;
	
	public void createTransactionList() {
		if(transactions == null) {
			transactions = new ArrayList<>();
		}
	}

	public boolean isJSONValid(String jsonInString) {
	    try {
	       final var mapper = new ObjectMapper();
	       mapper.readTree(jsonInString);
	       return true;
	    } catch (IOException e) {
	       return false;
	    }
	}
	
	
	private BigDecimal parseAmount(JSONObject transaction) {
		return new BigDecimal((String) transaction.get("valor"));
	}
	
	private LocalDateTime parseTransactionDate(JSONObject transaction) {
		String transactionDate = (String) transaction.get("dataHora");
		LocalDateTime dateTime = LocalDateTime.parse(transactionDate);
		return dateTime;
	}
	
	public boolean isTransactionInFuture(Transaction transaction) {
		return transaction.getDataHora().isAfter(LocalDateTime.now());
	}
	
	public boolean isTransactionBiggerThanZero(Transaction transaction) {
		if(transaction.getValor().compareTo(BigDecimal.ZERO) >=0 ) {
			return true;
		}else {
			return false;
		}
	}
	
	private void setTransactionValues(JSONObject jsonTransaction, Transaction transaction) {	
		transaction.setValor(jsonTransaction.get("valor") != null ? parseAmount(jsonTransaction) : transaction.getValor());
		transaction.setDataHora(jsonTransaction.get("dataHora") != null ? 
				parseTransactionDate(jsonTransaction) : transaction.getDataHora());
	}
	
	
	public Transaction create(JSONObject jsonTransaction) {
		Transaction transaction = new Transaction();
		setTransactionValues(jsonTransaction, transaction);
		return transaction;
	}
	
	
	public void add(Transaction transaction) {
		createTransactionList();
		transactions.add(transaction);
	}
	
	public List<Transaction> find() {
		createTransactionList();
		return transactions;
	}
	
	public boolean delete() {
		transactions.clear();
		return transactions.isEmpty();
	}

}
