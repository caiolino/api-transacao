package com.financeapi.financeapi.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Transaction {
	
	private BigDecimal valor;
	
	private LocalDateTime dataHora;
	
	private LocalDateTime transactionDateEntry = LocalDateTime.now();
	
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public LocalDateTime getDataHora() {
		return dataHora;
	}
	public void setDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
	}
	public LocalDateTime getTransactionDateEntry() {
		return transactionDateEntry;
	}
	public void setTransactionDateEntry(LocalDateTime transactionDateEntry) {
		this.transactionDateEntry = transactionDateEntry;
	}
	
}
