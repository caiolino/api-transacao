package com.financeapi.financeapi.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.financeapi.financeapi.model.Statistic;
import com.financeapi.financeapi.model.Transaction;
import com.financeapi.financeapi.service.StatistcService;
import com.financeapi.financeapi.service.TransactionService;

@RestController
@RequestMapping("/estatistica")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StatisticController {
	
	private static final Logger logger = Logger.getLogger(StatisticController.class);
	
	@Autowired
	private StatistcService statisticService;
	
	@Autowired
	private TransactionService transactionService;
	
	@GetMapping()
	public ResponseEntity<Statistic> getStatistics() {
		List<Transaction> transactions = transactionService.find();
		transactions = statisticService.getAllTransactionsLastMinute(transactions);
		logger.info(statisticService.statsCreate(transactions));
		return ResponseEntity.ok(statisticService.statsCreate(transactions));
	}

}
