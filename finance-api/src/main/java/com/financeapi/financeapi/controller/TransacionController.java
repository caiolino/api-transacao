package com.financeapi.financeapi.controller;

import java.net.URI;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.financeapi.financeapi.model.Transaction;
import com.financeapi.financeapi.service.TransactionService;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TransacionController {
	
	private static final Logger logger = Logger.getLogger(TransacionController.class);
	
	@Autowired
	private TransactionService transactionService;
	
	@GetMapping("/transacao")
	public ResponseEntity<List<Transaction>> find() {
		if(transactionService.find().isEmpty()) {
			return ResponseEntity.noContent().build(); 
		}
		logger.info(transactionService.find());
		return ResponseEntity.ok(transactionService.find());
	}
	
	@PostMapping("/transacao")
	@ResponseBody
	public ResponseEntity<Transaction> create(@RequestBody JSONObject transaction) {
		try {
			if(transactionService.isJSONValid(transaction.toString())) {
				Transaction transactionCreated = transactionService.create(transaction);
				URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/transacao").build().toUri();
				
				if(transactionService.isTransactionInFuture(transactionCreated)){
					logger.error("The transaction date is in the future.");
					return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(null);
				}else {

					if(transactionService.isTransactionBiggerThanZero(transactionCreated)) {
						transactionService.add(transactionCreated);
						return ResponseEntity.created(uri).body(null);
					}else {
						logger.error("The transaction value is smaller tha Zero..");
						return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(null);
					}
				}
				
			}else {
				return ResponseEntity.badRequest().body(null);
			}
		}catch(Exception e) {
			logger.error("ERRO: JSON fields are not parsable. " + e);
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(null);
		}
	}

	@DeleteMapping("/transacao")
	public ResponseEntity<Boolean> delete() {
		try {
			transactionService.delete();
			return ResponseEntity.noContent().build();
		}catch(Exception e) {
			logger.error(e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}

}
